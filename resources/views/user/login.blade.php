@extends('layouts.frontend_layout')

@section('title', 'Home')

@section('content')
	<div class="main-outer-wrapper">
		<div class="container">
			<div class="row">
				<div class="header-logo">
					<nav class="navbar navbar-inverse">
					  <div class="container-fluid">
					    <div class="" id="myNavbar">
					      	<ul class="nav navbar-nav">
						      	<li><a href="index.html"><img src="{{asset('images/logo.png')}}" class="logo-responsive"></a></li>
						     </ul>
					    </div>
					  </div>
					</nav>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="login-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
		            <h1 class="text-center login-title">Sign in to continue</h1>
		            <div class="account-wall login-div">
		                <img class="profile-img" src="{{asset('images/loginprofile.png')}}" alt="">
		                @if (\Session::has('error'))
		                <div class="col-md-12">
						    <div class="alert alert-danger">
						        <p>{!! \Session::get('error') !!}</p>
						    </div>
						</div>
						@endif
		                <form class="form-signin" id="before-login-form">
			                <div class="form-group">
			                	<input type="text" class="form-control" placeholder="code" required name="code" id="login-code">
			                </div>

			                <button class="btn btn-lg btn-primary btn-block" id="next-btn" type="button">Next</button>
			            </form>
		            </div>

		            <div class="account-wall player-div" style="display: none">
		                <img class="profile-img" src="{{asset('images/loginprofile.png')}}" alt="">
		                <form class="form-signin" action="{{url('postLogin')}}" method="post" id="login-form">
		                	@csrf
		                	<input type="hidden" name="code" id="append-code">
			                <div class="form-group">
			                	<input type="text" class="form-control" placeholder="Enter Players" required name="players">
			                </div>

			                <input  class="btn btn-lg btn-primary btn-block" type="submit" value="Login">
			            </form>
		            </div>
		        </div>
			</div>
		</div>
	</div>
@endsection

@section('js')
<script>
$(function() {

	$("#login-form").validate({
        rules: {
          players: "required",
        },
        messages: {
          players: "Please enter a players.",
        }
    });

    $("#before-login-form").validate({
        rules: {
          code: "required",
        },
        messages: {
          code: "Please enter a code.",
        }
    });

    $("#next-btn").click(function(){
	    $("#before-login-form").valid();

	    if($("#before-login-form").valid()) {
	    	var code = $("#login-code").val();
	    	$("#append-code").attr('value', code);
	    	$(".login-div").hide();
	    	$(".player-div").show();
    	}
    });

});
</script>
@stop