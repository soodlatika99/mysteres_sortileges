@extends('layouts.frontend_layout')

@section('title', 'Home')

@section('content')


	<div class="container">
		
		<div class="main-outer-wrapper">
			<div class="container">
				<div class="row">
					<div class="header-logo">
						<nav class="navbar navbar-inverse">
						  <div class="container-fluid">
						    <div class="" id="myNavbar">
						      	<ul class="nav navbar-nav">
							      	<li><a href="{{url('/')}}"><img src="{{asset('images/logo.png')}}" class="logo-responsive"></a></li>
							     </ul>
						    </div>
						  </div>
						</nav>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>

	</div>
	

	<div class="container full">

		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<h4 class="gametitle">Dates à venir</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="col-md-6 col-xs-6 col-sm-6">
					<div class="dates-wrapper">
						<div class="inner-wrap">
							<p><img src="images/calendar.png"></p>
							<p class="dates">April - 6/04/2019</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-xs-6 col-sm-6">
					<div class="dates-wrappert">
						<div class="inner-wrap">
							<p><img src="images/calendar.png"></p>
							<p class="dates">April - 6/04/2019</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="log-wrapper">
		<div class="container">
			<div class="row Ptop">
				<div class="col-md-6 col-xs-12 col-sm-6 col-xs-6">
					<div class="card-wrapper">
						<div class="card-inner">
							<p><img src="images/icons8-male.png"></p>
							<p><a href="{{url('login')}}">Accédez à votre espace joueur </a></p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-xs-12 col-sm-6 col-xs-6">
					<div class="card-wrapper">
						<div class="card-inner">
							<p><img src="images/registration.png"></p>
							<p><a href="#">Pas encore inscrit ?</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
