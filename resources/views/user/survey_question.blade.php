@extends('layouts.frontend_layout')

@section('title', 'Clues')

@section('content')
	<div class="main-outer-wrapper">
		<div class="container">
			<div class="row">
				<div class="header-logo">
					<nav class="navbar navbar-inverse">
					  <div class="container-fluid">
					    <div class="" id="myNavbar">
					      	<ul class="nav navbar-nav">
						      	<li><a href="{{url('user/home')}}"><img src="{{asset('images/logo.png')}}" class="logo-responsive"></a></li>
						      	<li class="homeright"><a href="{{url('user/home')}}" type="button" class="btn btn-sm btn-primary btn-home"><i class="fa fa-home" aria-hidden="true"></i></a></li>
						    </ul>
					    </div>
					  </div>
					</nav>
				</div>
			</div>
		</div>
	</div>	

	<div class="container">
		<div class="row Ptop">
			<div class="col-md-6 col-md-offset-3">
				<div class="clues-box">
					<div class="clues-inner">
					   
						<form action="{{route('submitsurvey')}}" method="post" id="surveyquestions">
							{{csrf_field()}}
							{{--
							<div style="margin:20px">
							    <span id="timer" style="font-size: 18px;font-weight: 600;">
								00:<span id="time" style="font-size: 18px;font-weight: 600;">00</span>     
								 </span>
							</div>
							--}}

							@foreach($questions as $question)
							
							<div class="row divques" id="next{{$question->id}} question{{$question->id}}">
								
								<input type="hidden" class="svquestion" name="squestion[{{$question->id}}]" value="{{$question->id}}">
						        <div class="col-md-12" id="panel1">
						            <div class="panel panel-default">
						                <div class="panel-heading" style="height: 60px;padding: 20px;">
						                    <h3 class="panel-title text-danger questiontext{{$question->id}}" data-right="{{$question->rightoption}}">
						                        <i class="fa fa-question-circle fa-1.8x"></i>{{$question->question}}   

						                    </h3>
						                </div>
						                <div class="panel-body two-col" style="height:233px;overflow: overlay;">
						                    <div class="row" style="padding: 20px 0 20px 0">
						                        <div class="col-md-6" style="margin-top: 10px;">
						                            <div class="frb frb-danger margin-bottom-none">
							    						<input type="radio" id="radio_ans{{$question->id}}" name="sanswer[{{$question->id}}]" value="option1" class="quesoption" required>
							    						<label for="radio_ans01">
							    							<span class="frb-title">{{$question->option1}}</span>
							    							<span class="frb-description"></span>
							    						</label>
						    					    </div>
						                        </div>
						                        <div class="col-md-6" style="margin-top: 10px;">
						                            <div class="frb frb-danger margin-bottom-none">
							    						<input type="radio" id="radio_ans{{$question->id}}" name="sanswer[{{$question->id}}]" value="option2" class="quesoption" required>
							    						<label for="radio_ans02">
							    							<span class="frb-title">{{$question->option2}}</span>
							    							<span class="frb-description"></span>
							    						</label>
						    					    </div>
						                        </div> 
						                        <div class="col-md-6" style="margin-top: 10px;">
						                            <div class="frb frb-danger margin-bottom-none">
							    						<input type="radio" id="radio_ans{{$question->id}}" name="sanswer[{{$question->id}}]" value="option3" class="quesoption" required>
							    						<label for="radio_ans02">

							    							<span class="frb-title">{{$question->option3}}</span>
							    							<span class="frb-description"></span>
							    						</label>
						    					    </div>
						                        </div> 
						                        <div class="col-md-6" style="margin-top: 10px;">
						                            <div class="frb frb-danger margin-bottom-none">
							    						<input type="radio" id="radio_ans{{$question->id}}" name="sanswer[{{$question->id}}]" value="option4" class="quesoption" required>
							    						<label for="radio_ans02">

							    							<span class="frb-title">{{$question->option4}}</span>
							    							<span class="frb-description"></span>
							    						</label>
						    					    </div>
						                        </div> 

						                    </div>
						        
								        </div>
								   </div>
								</div>
								
					        </div>	
					        @endforeach	
					       
					            <div class="panel-footer">
				                    <div class="row">
				                        <div class="col-md-6">
				                            <button type="button" class="btn btn-success btn-sm btn-block hidden">
				                            <span class="fa fa-send"></span>back</button>
				                        </div>
				                        <div class="col-md-6">
				                            <button id="nextbtn" type="button" class="btn btn-primary btn-sm btn-block shownum_class">
				                            next</button>
				                        </div>
				                    </div>
								</div>
									
						</form>	
								
					</div>
				</div>
			</div>
		</div>
	</div>



@section('js')
<script type="text/javascript">
		/*
		var interval;
	    function settimer(ques)
	    {
			var counter = 10;

			interval = setInterval(function() {
			    counter--;
			    // Display 'counter' wherever you want to display it.
			    if (counter <= 0) {
			     	clearInterval(interval);
			      	// $('#timer').html("<h3>1:00</h3>");  
			        setagain(ques);
			        return; 
			        exit();
			    }else{
			    	$('#time').text(counter);
			        //Console.log("Timer --> " + counter);
			    }
			}, 1000);	
	    }

	    function setagain(ques)
	    {
	    	console.log(ques);
	    	nextone = ques.next();
	    	if(nextone.hasClass('divques'))
	    	{
	    		$(".divques").hide().removeClass('current');
	    		nextone.show().addClass('current');
	    		settimer(nextone);
	    	}
	    }

	    function settimeout(el)
	    {
	    	setTimeout(
			  function() 
			  {
			    //do something special
			    $(el).show();
			  }, 10000);
	    }


		$(document).ready(function() {
				$(".divques").hide();
				var question = $(".divques").first().show();
				question.addClass('current');
				settimer(question);
				 
				$("#nextbtn").on('click', function(event) {
					event.preventDefault();
					var current = $("div.divques.current");
					nextelem = current.next();
					if(nextelem.hasClass('divques'))
					{
						clearInterval(interval);
						current.hide();
						nextelem.show();
						settimer(nextelem);
					}
				});


		});	

		*/

		jQuery(document).ready(function($) {
			$(".divques").hide();
			$(".divques").first().show().addClass('current');
			$("#nextbtn").prop('disabled', true);

			$("#nextbtn").on('click', function(event) {
				event.preventDefault();
				/* Act on the event */
				var current = $("div.divques.current");
				var next = $("div.divques.current").next();
				if(next.hasClass('divques'))
				{
					var question = current.children('.svquestion').val();
					var answer = $("input[name='sanswer["+question+"]']:checked").val();
					console.log(answer);
					$.ajax({
						url: '{{route("savesurveyrunning")}}',
						type: 'post',
						dataType: 'json',
						data: {'_token': "{{csrf_token()}}",'question':question,'answer':answer}
					})
					.done(function(data) {
						console.log(data);
						console.log("success");
						$("#nextbtn").prop('disabled', true);
					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});
					
					current.removeClass('current').hide();
					next.addClass('current').show();
				}else{
					$("#surveyquestions").submit();
				}

			});

			$(".quesoption").on('click', function(event) {
				/* Act on the event */
				$("#nextbtn").prop('disabled', false);
			});
		});
</script>		
@endsection

