@extends('layouts.frontend_layout')

@section('title', 'SurveyCompleted')


@section('content')
	<div class="main-outer-wrapper">
		<div class="container">
			<div class="row">
				<div class="header-logo">
					<nav class="navbar navbar-inverse">
					  <div class="container-fluid">
					    <div class="" id="myNavbar">
					      	<ul class="nav navbar-nav">
						      	<li><a href="{{url('user/home')}}"><img src="{{asset('images/logo.png')}}" class="logo-responsive"></a></li>
						      	<li class="homeright"><a href="{{url('user/home')}}" type="button" class="btn btn-sm btn-primary btn-home">X</a></li>
						    </ul>
					    </div>
					  </div>
					</nav>
				</div>
			</div>
		</div>

	</div>	
	
	<div class="container">
		<div class="row Ptop">
			<h1 class="text-center">{{$message}}</h1>
		</div>
	</div>
@endsection
