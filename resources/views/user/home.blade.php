@extends('layouts.frontend_layout')

@section('title', 'Home')

@section('content')

	<div class="main-outer-wrapper">
		<div class="container">
			<div class="row">
				<div class="header-logo">
					<nav class="navbar navbar-inverse">
					  <div class="container-fluid">
					    <div class="" id="myNavbar">
					      	<ul class="nav navbar-nav">
						      	<li><a href="{{url('user/home')}}"><img src="{{asset('images/logo.png')}}" class="logo-responsive"></a></li>
						     </ul>
					    </div>
					  </div>
					</nav>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="container palyerswrap">
		<div class="row Ptop">
			<div class="welcome-note">

				<p>
					Bienvenue, élève de <b style="color:#c19850;"><i>{{Auth::user()->team['name']}}</i></b> ! Vous avez actuellement {{$tpoints}} points.
				</p>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row Pdtop">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<a href="{{url('user/codes')}}" class="bxlink">
						<div class="codes-wrap">
							<p>codes</p>
						</div>
					</a>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<a href="{{url('user/collections')}}" class="bxlink">
						<div class="codes-wrap">
							<p>Collectionneur</p>
						</div>
					</a>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row Pdtop">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<a href="{{url('user/archive')}}" class="bxlink">
						<div class="codes-wrap">
							<p>les archives</p>
						</div>
					</a>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6 notification">
                        		@if(count($diffarray)>0)
								<span class="badge new">New</span>
								@endif
					<a href="{{url('user/messages')}}" class="bxlink">
						<div class="codes-wrap">	
							 <p>Messagerie</p>	
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>

@endsection