@extends('layouts.frontend_layout')

@section('title', 'Clues')

@section('content')
	<div class="main-outer-wrapper">
		<div class="container">
			<div class="row">
				<div class="header-logo">
					<nav class="navbar navbar-inverse">
					  <div class="container-fluid">
					    <div class="" id="myNavbar">
					      	<ul class="nav navbar-nav">
						      	<li><a href="{{url('user/home')}}"><img src="{{asset('images/logo.png')}}" class="logo-responsive"></a></li>
						      	<li class="homeright"><a href="{{url('user/home')}}" type="button" class="btn btn-sm btn-primary btn-home"><i class="fa fa-home" aria-hidden="true"></i></a></li>
						    </ul>
					    </div>
					  </div>
					</nav>
				</div>
			</div>
		</div>
	</div>	
	
	<div class="container">
		<div class="row Ptop">
			<div class="col-md-6 col-md-offset-3">
				<div class="clues-box">
					<div class="clues-inner">
						<form>
							<div class="form-group">
								<p class="search-box">{{$clue->title}}</p>
								<a href="{{url('user/codes')}}"><span id="searchclear" class="fa fa-close"></span></a>
							</div>
							@if(!empty($clue->clue))
							<div class="form-group">
								<p class="customtextarea">
									{!! $clue->clue !!}
								</p>
							</div>
							@endif
							@if(!empty($clue->image))
							<div class="form-group">
								<div class="main-img-preview">
					                <img class="thumbnail img-preview" src="{{asset('uploads/'.$clue->image)}}" title="Preview Logo">
					            </div>
							</div>
							@endif
							<div class="form-group">
								{{--<button type="button" class="btn btn-primary add-archive pull-right">Archive Added</button>--}}
								Indice archivé
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection