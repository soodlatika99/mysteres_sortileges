@extends('layouts.frontend_layout')

@section('title', 'Codes')

@section('content')
	<div class="main-outer-wrapper">
		<div class="container">
			<div class="row">
				<div class="header-logo">
					<nav class="navbar navbar-inverse">
					  <div class="container-fluid">
					    <div class="" id="myNavbar">
					      	<ul class="nav navbar-nav">
						      	<li><a href="{{url('user/home')}}"><img src="{{asset('images/logo.png')}}" class="logo-responsive"></a></li>
						      	<li class="homeright"><a href="{{url('user/home')}}" type="button" class="btn btn-sm btn-primary btn-home"><i class="fa fa-home" aria-hidden="true"></i></a></li>
						    </ul>
					    </div>
					  </div>
					</nav>
				</div>
			</div>
		</div>

	</div>	
	
	<div class="container">
		<div class="row Ptop">
			<div class="col-md-6 col-md-offset-3">
				<div class="cod-wrap">
					<div class="card card-container"> 
						@if (\Session::has('error'))
		                <div class="col-md-12">
						    <div class="alert alert-danger">
						        <p>{!! \Session::get('error') !!}</p>
						    </div>
						</div>
						@endif
			       		<form class="cod-form" action="{{url('user/submitCode')}}" method="post">
			       			@csrf
			                <span id="reauth-email" class="reauth-email"></span>
			                <p class="input_title">Code de l'indice</p>
			                <div class="form-group">
			                	<input type="text" class="form-control" placeholder="code" required name="code">
			                </div>
			                
			                <button class="btn btn-lg btn-primary pull-right submit" type="submit">Valider</button>
			            </form>
			        </div>
				</div>
			</div>
		</div>
	</div>		
@endsection