@extends('layouts.frontend_layout')

@section('title', 'Archive')

@section('css')
<style>
.detail-clue {
    padding: 10px;
    border: 1px solid;
    margin-bottom: 10px;
}
.wrapborder {
	min-height: auto;
}
</style>
@stop

@section('content')
	<div class="main-outer-wrapper">
		<div class="container">
			<div class="row">
				<div class="header-logo">
					<nav class="navbar navbar-inverse">
					  <div class="container-fluid">
					    <div class="" id="myNavbar">
					      	<ul class="nav navbar-nav">
						      	<li><a href="{{url('user/home')}}"><img src="{{asset('images/logo.png')}}" class="logo-responsive"></a></li>
						      	<li class="homeright"><a href="{{url('user/home')}}" type="button" class="btn btn-sm btn-primary btn-home">X</a></li>
						    </ul>
					    </div>
					  </div>
					</nav>
				</div>
			</div>
		</div>

	</div>	
	
	<div class="container">
		<div class="row Ptop">
			<h1 class="text-center archive-title">Indice</h1>
			<div class="col-md-6 col-md-offset-3">
				<div class="clues-box">
					<div class="clues-inner">
						<div class="wrapborder">
							<div class="form-group">
								@foreach($archives as $archive)
								<div class="cldiv">
									<p class="cluesopt"> - {{$archive->clue->title}}</p>
									<div class="detail-clue" style="display: none">
										@if(!empty($archive->clue->image))
											<img src="{{asset('uploads/'.$archive->clue->image)}}" class="img-thumbnail" width="100px" height="100px">
										@endif
										<p>{!!$archive->clue->clue!!}</p>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
<script>
$(function() {
	$(".cluesopt").click(function(){
		$(this).nextAll(".detail-clue").toggle();
	});
});
</script>
@stop