@extends('layouts.frontend_layout')

@section('title', 'Collections')

@section('content')


	<div class="main-outer-wrapper">
		<div class="container">
			<div class="row">
				<div class="header-logo">
					<nav class="navbar navbar-inverse">
					  <div class="container-fluid">
					    <div class="" id="myNavbar">
					      	<ul class="nav navbar-nav">
						      	<li><a href="{{url('user/home')}}"><img src="{{asset('images/logo.png')}}" class="logo-responsive"></a></li>
						      	<li class="homeright"><a href="{{url('user/home')}}" type="button" class="btn btn-sm btn-primary btn-home">X</a></li>
						    </ul>
					    </div>
					  </div>
					</nav>
				</div>
			</div>
		</div>
	</div>	
	
	<div class="container">
		<div class="row Ptop">
			<div class="col-md-6 col-md-offset-3">
				<div class="clues-box">
					<div class="clues-inner">
						<?php $count = 2; ?>
						@foreach($collections as $collection)
							<div class="col-md-6 col-xs-6">
								    <div class="img-gal">
										<img src="{{asset('uploads/'.$collection->clue->image)}}" class="img-thumbnail thumbopen"> 
									</div>
							</div>
							
							@if($count%2 == 1)
							    <div class="clearfix"></div>
								<div class="spacerm"></div>
							@endif
						    <?php $count++; ?>
						@endforeach

						@if(count($collections) == 0)
							<?php $fackCollections = [1,2,3,4] ?>
							@foreach($fackCollections as $collection)
							<div class="col-md-6 col-xs-6">
								<div class="img-gal">
									<img src="{{asset('images/placeholder.jpg')}}" class="img-thumbnail thumbopen"> 
								</div>
							</div>
							
							@if($count%2 == 1)
						    <div class="clearfix"></div>
							<div class="spacerm"></div>
							@endif
						  
						    <?php $count++; ?>
							@endforeach
                        @endif
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      
	      <div class="modal-body" style="text-align: center;">
	        <img src="" width="380px" id="openimage" >
	      </div>
	      
	    </div>
	  </div>
	</div>

	@section('js')
		<script>
			$(document).ready(function() {
			    $( ".thumbopen" ).on( "click", function() {
				  	$(".modal #openimage").attr('src',$(this).attr('src'));
				  	$('#exampleModal').modal('show');
				});
			});

			
		</script>
	@endsection

@endsection