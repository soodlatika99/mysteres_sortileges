@extends('layouts.frontend_layout')

@section('title', 'Messages')

@section('content')
	<div class="main-outer-wrapper">
		<div class="container">
			<div class="row">
				<div class="header-logo">
					<nav class="navbar navbar-inverse">
					  <div class="container-fluid">
					    <div class="" id="myNavbar">
					      	<ul class="nav navbar-nav">
						      	<li><a href="{{url('user/home')}}"><img src="{{asset('images/logo.png')}}" class="logo-responsive"></a></li>
						      	<li class="homeright"><a href="{{url('user/home')}}" type="button" class="btn btn-sm btn-primary btn-home">X</a></li>
						    </ul>
					    </div>
					  </div>
					</nav>
				</div>
			</div>
		</div>
	</div>	
	
	<div class="container">
		<div class="row PMtop">
			<div class="col-md-6 col-md-offset-3">
				<div class="clues-box">
					<div class="clues-inner">
						<div class="col-md-12 col-xs-12">
							@foreach($messages as $message)
							<div class="message-cart fimsg">
								<p class="msg-date">{{Carbon\Carbon::parse($message->date)->format('d-m')}} 
								&nbsp;&nbsp;
								<?php \Carbon\carbon::setLocale('fr'); ?>
								{{$message->created_at->diffForHumans()}}
								<i class="fa fa-envelope-o pull-right" aria-hidden="true"></i></p>
								<p class="msg-title">{{$message->title}}</p>
								<p class="msg-content">
									{!! $message->description !!}
								</p>
							</div>
							@endforeach
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection