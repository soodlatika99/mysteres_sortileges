 <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>M&S</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Mystères&Sortilèges</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{asset('images/loginprofile.png')}}" class="user-image" alt="Image">
              <input type="hidden" id="profile_id" value="{{Auth::user()->id}}">
              <span class="hidden-xs"> {{Auth::user()->email}} <span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{asset('images/loginprofile.png')}}" class="img-circle" alt="Image">
                <!-- <img src="" class="img-circle" alt="Image"> -->
                <p>
                 {{ Auth::user()->email }}
                </p>
              </li>
              <!-- Menu Body -->
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ url('admin/logout') }}" class="btn btn-default btn-flat">Sign Out</a>
                </div>
              </li>
            </ul>
          </li>         
            
        </ul>
      </div>
    </nav>
  </header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">     
        <ul class="sidebar-menu"> 
          <li class="active treeview">
              <a href="{{ url('admin/dashboard') }}">
              <i class="fa fa-dashboard"></i> <span>Tableau de bord</span>
              </a>
          </li>   
          <li>
            <a href="{{ url('admin/codes') }}">
              <i class="fa fa-tag" aria-hidden="true"></i>
              <span>Codes joueurs</span>
              <span class="pull-right-container">
              </span> 
            </a>
          </li>
          <li>
            <a href="{{ url('admin/teams') }}">
              <i class="fa fa-user" aria-hidden="true"></i>
              <span>Teams</span>
              <span class="pull-right-container">
              </span> 
            </a>
          </li>
          <li>
            <a href="{{ url('admin/surveystatistics') }}">
              <i class="fa fa-user" aria-hidden="true"></i>
              <span>Survey Statistics</span>
              <span class="pull-right-container">
              </span> 
            </a>
          </li>
          <li>
            <a href="{{ url('admin/clues') }}">
              <i class="fa fa-list-alt" aria-hidden="true"></i>
              <span>Indices</span>
              <span class="pull-right-container">
              </span> 
            </a>
          </li>
          <li>
            <a href="{{ url('admin/message') }}">
              <i class="fa fa-envelope" aria-hidden="true"></i>
              <span>Messages</span>
              <span class="pull-right-container">
              </span> 
            </a>
          </li>
        </ul>
    </section>
           
    <!-- /.sidebar -->
  </aside>