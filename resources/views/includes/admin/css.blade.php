
<link rel="stylesheet" href="{{ asset('admin/css/custom.css') }}"/>

<link rel="stylesheet" type="text/css" href="{{asset('css/admin_style.css')}}">

<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="{{ asset('admin/bootstrap/css/bootstrap.min.css') }}"/>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('admin/dist/css/AdminLTE.min.css') }}"/>

<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->

<link rel="stylesheet" href="{{ asset('admin/dist/css/skins/_all-skins.min.css') }}"/>

<!-- dataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}"/>

<!-- DatePicker -->
<link rel="stylesheet" type="text/css" href="{{asset('css/datepicker.min.css')}}">