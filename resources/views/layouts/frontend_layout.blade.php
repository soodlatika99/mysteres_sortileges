<!DOCTYPE html>
<html>
<head>
	<title>Mystères&Sortilèges : @yield('title')</title>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	@include('includes.css')
  	@section('css')
	@show
</head>
<body>
@section('content')

@show

@include('includes.footer')

@include('includes.js')

@section('js')
@show

</body>
</html>