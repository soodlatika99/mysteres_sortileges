<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://opengraphprotocol.org/schema/">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{{'DASHBOARD'}}</title>
		<meta name="keywords" content="ColisDays" />
		<meta name="description" content="ColisDays" />
		<meta name="robots" content="index, follow" />
		<meta property="og:site_name" content="ColisDays" />
		<meta property="og:description" content="ColisDays" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta charset="UTF-8" />
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/bundles/jamain/img/favicons/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		@include('includes.admin.css')

		@section('css')

		@show
		<style type="text/css">
			.invalid-feedback strong {
		        color: red;
		      }
		</style>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	</head>

	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			@include('includes.admin.header')

			@section('content')
				
			@show

			@include('includes.admin.footer')   

			@include('includes.admin.js')

			@section('js')
			
			@show
		</div>
	</body>
</html>

