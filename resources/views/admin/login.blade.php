@extends('layouts.frontend_layout')

@section('title', 'Admin Login')

@section('content')
	<div class="login-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
		            <h1 class="text-center login-title">Admin Login</h1>
		            <div class="account-wall">
		                <img class="profile-img" src="{{asset('images/loginprofile.png')}}" alt="">
		                @if (\Session::has('error'))
					    <div class="alert alert-danger">
					        <p>{!! \Session::get('error') !!}</p>
					    </div>
						@endif
		                <form class="form-signin" action="{{url('admin/postLogin')}}" method="post">
		                	@csrf
			                <div class="form-group">
			                	<input type="text" class="form-control" placeholder="email" required autofocus name="email">
			                </div>

			                <div class="form-group">
			                	<input type="password" class="form-control" placeholder="password" required autofocus name="password">
			                </div>

			                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
			            </form>
		            </div>
		        </div>
			</div>
		</div>
	</div>
@endsection