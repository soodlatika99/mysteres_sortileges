@extends('layouts/admin.frontend_layout')

@section('content')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Survey Statistics</h1>
        </section>

        

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Survey Statistics</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <form action="{{url('admin/surveystatistics')}}" method="get" accept-charset="utf-8" id="surveysubmit">
                                <div class="form-group">
                                    <label for="clueid" >Select Survey</label>
                                    <select name="clues" class="form-control" id="clues">
                                        <option value=""></option>
                                        @foreach($clues as $clue)
                                            <option value="{{$clue->id}}" @if($clue->id==$clueid) selected @endif>{{$clue->code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </form><br>

                            <div class="table-responsive">
                            <table id="example1" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Question Number</th>
                                        <th>Number of users Answered</th>
                                        <th>Correct Answerd</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($statisticsarray as $question=>$statistics)
                                        <tr>
                                            <td>{{$statistics['question']}}</td>
                                            <td>{{$statistics['users_answered']}}</td>
                                            <td>{{$statistics['correct_answers']}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>

                        </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
<!-- /.content-wrapper --> 
@endsection

@section('js')
    <script>
        jQuery(document).ready(function($) {
            $("#clues").on('change', function(event) {
                event.preventDefault();
                if($(this).val())
                {
                    $("#surveysubmit").submit(); // 
                }
            });
        });
    </script>
@endsection