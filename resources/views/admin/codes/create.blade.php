@extends('layouts/admin.frontend_layout')

@section('css')
<style>
form#user-form {
    padding: 30px;
}
</style>
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Codes</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body">
                        <div class="box box-info">
                            <div class="box-header with-border">
                              <h3 class="box-title">Create Code</h3>

                                <a href="{{ url('/admin/codes') }}" title="Back"><button class="btn btn-warning btn-sm pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            </div>

                            <form method="POST" action="{{ url('/admin/codes') }}" accept-charset="UTF-8" enctype="multipart/form-data" id="user-form">
                                {{ csrf_field() }}

                                @include ('admin.codes.form', ['formMode' => 'create'])
                            </form>                    
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
<script>
$(function() {

    $("#user-form").validate({
        rules: {
            name: "required",
            email: "required",
            code: "required",
        },
        messages: {
            name: "Please enter a name.",
            email: "Please enter a email.",
            code: "Please enter a code.",
        }
    });

});
</script>
@stop