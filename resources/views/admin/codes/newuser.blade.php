@extends('layouts/admin.frontend_layout')

@section('css')
<style>
form#user-form {
    padding: 30px;
}
</style>
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Codes</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body">
                        <div class="box box-info">
                            <div class="box-header with-border">
                              <h3 class="box-title">Create Codes</h3>

                                <a href="{{ url('/admin/codes') }}" title="Back"><button class="btn btn-warning btn-sm pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            </div>

                            <form method="POST" action="{{ url('/admin/newcodes') }}" accept-charset="UTF-8" enctype="multipart/form-data" id="user-form">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <input type="number" name="nouser" min="1" max="50" class="form-control">
                                </div>

                                <div class="form-group">
                                    <input type="submit" name="usercodes" class="btn btn-primary" value="Create Codes">
                                </div>      
                            </form>                    
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
<script>

</script>
@stop