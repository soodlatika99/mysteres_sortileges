@extends('layouts/admin.frontend_layout')

@section('content')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Codes joueurs</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            @if (session('success'))
            <div class="alert alert-success alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <p>{{ session('success') }}</p>
            </div>
            @endif
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Codes joueurs</h3>
                            <a href="{{ url('admin/userexcel') }}" title="Download" style="margin-left:10px;"><button class="btn btn-primary pull-right" style="margin-left:10px;"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download</button></a>
                            <a href="{{ url('admin/codes/create') }}" title="Add"><button class="btn btn-primary pull-right"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Créer</button></a>
                            <a href="{{ url('admin/createuser') }}" title="Add"><button class="btn btn-primary pull-right"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Créer Codes</button></a>&nbsp;
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                            <table id="example1" class="table table-striped">
                                <thead>
                                    <tr>
                                        <!-- <th>Name</th>
                                        <th>Email</th> -->
                                        <th>Equipe</th>
                                        <th>Code</th> 
                                        <!--<th>Points</th>-->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                @foreach($users as $user)
                                <tbody>
                                    <tr>

                                        <td>{{$user->team->name}}</td>
                                        <td>{{$user->code}}</td>
                                        {{-- td>{{$user->points}}</td> --}}
                                        <td>
                                            <a href="{{url('admin/codes/'.$user->id.'/edit')}}" class="btn btn-primary">Editer</a>
                                        </td>
                                        <td>
                                            <form action="{{url('admin/codes/'.$user->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Supprimer</button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                                @endforeach
                            </table>
                            </div>
                            {{ $users->links() }}
                        </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
<!-- /.content-wrapper --> 
@endsection