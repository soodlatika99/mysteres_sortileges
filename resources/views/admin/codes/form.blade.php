<!-- <div class="form-group">
    <label for="name">Name :</label>
    <input type="text" name="name" class="form-control" value="@if(!empty($user)){{$user->name}}@endif" required>
</div>

<div class="form-group">
    <label for="email">Email :</label>
    <input type="email" name="email" class="form-control" value="@if(!empty($user)){{$user->email}}@endif" required>
</div> -->

<div class="form-group">
    <label for="code">Code :</label>
    <input type="text" name="code" class="form-control" value="@if(!empty($user)){{$user->code}}@endif" required>
</div>

<div class="form-group">
    <label for="code">Equipe :</label>
    <select name="team" class="form-control">
    	@foreach($teams as $team)
    		<option value="{{$team->id}}" @if(!empty($user) && $user->team_id == $team->id) selected @endif>{{$team->name}}</option>
    	@endforeach
    </select>
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Mettre à jour' : 'Créer' }}">
</div>
<br>