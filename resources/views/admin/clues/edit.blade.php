@extends('layouts/admin.frontend_layout')

@section('css')
<style>
form {
    padding: 30px;
}

</style>
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Codes</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body">
                        <div class="box box-info">
                            <div class="box-header with-border">
                              <h3 class="box-title">Editer Indice</h3>

                                <a href="{{ url('/admin/clues') }}" title="Back"><button class="btn btn-warning btn-sm pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                            </div>

                            <form method="POST" action="{{ url('/admin/clues/'.$clue->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" id="clue-edit-form">
                                {{ csrf_field() }}
                                @method('PUT')

                                @include ('admin.clues.editform', ['formMode' => 'edit'])
                            </form>                    
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
<script>
$(function() {

    $('#summernote').summernote();

    $("#clue-edit-form").validate({
        rules: {
          code: "required",
          title: "required",
          ctype: "required"
        },
        messages: {
          code: "Please enter a code.",
          title: "Please enter a title.",
          ctype: "Please select clue type"
        }
    });
});

jQuery(document).ready(function($) {
    $("#summernotediv").hide();
    $("#imagediv").hide();

    function ctypecheckval()
    {
        /* Act on the event */
        var selval = $("#ctype").val();
            // alert(selval);
            if(selval=="text")
            {
                $("#summernotediv").show();
                $(".clupointdiv").show();
                $(".winorlossdiv").show();
                $("#imagediv").hide();
                $(".surveybox").hide();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").hide();

                $("#summernote").prop("required",true);
                $("#image").prop("required",false);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', false);

            }else if(selval=="image"){
                $("#summernotediv").hide();
                $(".clupointdiv").show();
                $("#imagediv").show();
                $(".winorlossdiv").show();
                $(".surveybox").hide();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").hide();

                $("#summernote").prop("required",false);
                $("#image").prop("required",true);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', false);

            }else if(selval=="textimage"){
                $("#summernotediv").show();
                $(".clupointdiv").show();
                $("#imagediv").show();
                $(".winorlossdiv").show();
                $(".surveybox").hide();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").hide();

                $("#summernote").prop("required",true);
                $("#image").prop("required",true);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', false);

            }else if(selval=="survey"){
                $("#summernotediv").hide();
                $(".clupointdiv").hide();
                $("#imagediv").hide();
                $(".winorlossdiv").hide();
                $(".surveybox").show();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").show();

                $("#summernote").prop("required",false);
                $("#image").prop("required",false);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', true);
            }
    }

    ctypecheckval();

    $("#ctype").change(function(event) {
            /* Act on the event */
            var selval = $(this).val();
            // alert(selval);
            if(selval=="text")
            {
                $("#summernotediv").show();
                $(".clupointdiv").show();
                $(".winorlossdiv").show();
                $("#imagediv").hide();
                $(".surveybox").hide();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").hide();

                $("#summernote").prop("required",true);
                $("#image").prop("required",false);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', false);

            }else if(selval=="image"){
                $("#summernotediv").hide();
                $(".clupointdiv").show();
                $("#imagediv").show();
                $(".winorlossdiv").show();
                $(".surveybox").hide();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").hide();

                $("#summernote").prop("required",false);
                $("#image").prop("required",true);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', false);

            }else if(selval=="textimage"){
                $("#summernotediv").show();
                $(".clupointdiv").show();
                $("#imagediv").show();
                $(".winorlossdiv").show();
                $(".surveybox").hide();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").hide();

                $("#summernote").prop("required",true);
                $("#image").prop("required",true);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', false);

            }else if(selval=="survey"){
                $("#summernotediv").hide();
                $(".clupointdiv").hide();
                $("#imagediv").hide();
                $(".winorlossdiv").hide();
                $(".surveybox").show();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").show();

                $("#summernote").prop("required",false);
                $("#image").prop("required",false);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', true);

            }
        });
    $(document).on('blur', '.surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point', function(event) {            
            // $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").change(function(event) {
            console.log($(this).val());
            var question = $(".surveyquestion");
            var susans1 = $(".surveyanswer1");
            var susans2 = $(".surveyanswer2");
            var susans3 = $(".surveyanswer3");
            var susans4 = $(".surveyanswer4");
            var susright = $(".rightans");
            var suspoint = $(".point");
            var status = true;
            $(question).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            $(susans1).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            $(susans2).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            $(susans3).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            $(susans4).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            

            $(susright).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            $(suspoint).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            console.log(status);
            if(status==false)
            {
                console.log("false");
                $(".addmore").prop('disabled', true);
            }else{
                console.log("true");
                $(".addmore").prop('disabled', false);
            }

        });

     $(".addmore").click(function(event) {
            
            $(".surveyboxouter").append(`
                <div class="surveybox">

                <div class="form-group surveydiv">
                    <label for="surveyquestion">Clue Question :</label>
                    <input type="text" name="surveyquestion[]" class="form-control surveyquestion">
                </div>

                <div class="form-group surveydivans">
                    <input type="hidden" name="surveyid[]" value="">
                    <label for="surveyanswer1">Clue Option 1 :</label>
                    <input type="text" name="surveyanswer1[]" class="form-control surveyanswer1"><br>

                    <label for="surveyanswer2">Clue Option 2:</label>
                    <input type="text" name="surveyanswer2[]" class="form-control surveyanswer2">

                    <label for="surveyanswer1">Clue Option 3 :</label>
                    <input type="text" name="surveyanswer3[]" class="form-control surveyanswer3"><br>

                    <label for="surveyanswer2">Clue Option 4:</label>
                    <input type="text" name="surveyanswer4[]" class="form-control surveyanswer4">
                </div>

                <div class="form-group surveydivansright">
                    <label for="rightans">Right Option</label>
                    <select name="rightans[]" class="form-control rightans">
                        <option value="option1">Option 1</option>
                        <option value="option2">Option 2</option>
                        <option value="option3">Option 3</option>
                        <option value="option4">Option 4</option>
                    </select>
                </div>

                <div class="form-group surveydivpoint">
                    <label for="points">Points</label>
                    <input type="number" name="points[]" class="form-control point" value="0" min="0" max="100">
                </div>
                
            </div>
            `);

            $(this).prop('disabled', true)

        });

});
</script>
@stop