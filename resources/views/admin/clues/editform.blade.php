<div class="form-group cluediv">
    <label for="code">Code :</label>
    <input type="text" id="cluecode" name="code" class="form-control" value="@if(!empty($clue)){{$clue->code}}@endif" required>
</div>

<div class="form-group">
    <label for="title">Title :</label>
    <input type="text" name="title" class="form-control" value="@if(!empty($clue)){{$clue->title}}@endif" required>
</div>

<div class="form-group">
    <label for="ctype">Type :</label>
    <select name="ctype" class="form-control" id="ctype" required>
        <option value="selectType">Select type</option>
        <option value="text" @if($clue->type=="text") selected @endif>Text</option>
        <option value="image" @if($clue->type=="image") selected @endif>Image</option>
        <option value="textimage" @if($clue->type=="textimage") selected @endif>Text and Image</option>
        <option value="survey" @if($clue->type=="survey") selected @endif>Survey</option>
    </select>
</div>

<div class="form-group" id="summernotediv">
    <label for="clue">Clue :</label>
    <textarea id="summernote" name="clue">@if(!empty($clue)){{$clue->clue}}@endif</textarea>
</div>

@if(!empty($clue) && !empty($clue->image))
    <img src="{{asset('uploads/'.$clue->image)}}" height="50px">
    <input type="hidden" name="already_image" class="form-control" value="{{$clue->image}}">
@endif
<div class="form-group" id="imagediv">
    <label for="image">Clue Image :</label>
    <input type="file" name="image" class="form-control" id="image" class="image" accept="image/*">
    <span id="file_error"></span>
</div>
<div class="surveyboxouter">
    @foreach($clue->survey as $survey)
    <div class="surveybox">
        <div class="form-group surveydiv">
            <label for="surveyquestion">Clue Question :</label>
            <input type="text" name="surveyquestion[]" class="form-control surveyquestion" value="{{$survey->question}}">
        </div>

        <input type="hidden" name="surveyid[]" value="{{$survey->id}}">

        <div class="form-group surveydivans">
            <label for="surveyanswer1">Clue Option 1 :</label>
            <input type="text" name="surveyanswer1[]" class="form-control surveyanswer1" value="{{$survey->option1}}"><br>

            <label for="surveyanswer2">Clue Option 2:</label>
            <input type="text" name="surveyanswer2[]" class="form-control surveyanswer2" value="{{$survey->option2}}">

            <label for="surveyanswer1">Clue Option 3 :</label>
            <input type="text" name="surveyanswer3[]" class="form-control surveyanswer3"  value="{{$survey->option3}}"><br>

            <label for="surveyanswer2">Clue Option 4:</label>
            <input type="text" name="surveyanswer4[]" class="form-control surveyanswer4"  value="{{$survey->option4}}">
        </div>

        <div class="form-group surveydivansright">
            <label for="rightans">Right Option</label>
            <select name="rightans[]" class="form-control rightans">
                <option value="option1" @if($survey->rightoption=="option1") selected @endif>Option 1</option>
                <option value="option2" @if($survey->rightoption=="option2") selected @endif>Option 2</option>
                <option value="option3" @if($survey->rightoption=="option3") selected @endif>Option 3</option>
                <option value="option4" @if($survey->rightoption=="option4") selected @endif>Option 4</option>
            </select>
        </div>

        <div class="form-group surveydivpoint">
            <label for="points">Points</label>
            <input type="number" name="points[]" class="form-control point" value="{{$survey->points}}" min="0" max="100">
        </div>
    </div>
    @endforeach

</div>
@if($clue->type =="survey")
        <div class="form-group surveydivaddmore">
            <button type="button" name="addmore" id="addmore" class="btn btn-primary btn-sm addmore" disabled >Add more Survey Questions</button>
        </div>
    @endif  



<div class="form-group clupointdiv" @if($clue->type=='survey') style="display:none;" @endif>
    <label for="point">Points</label>
    <input type="number" name="point" class="form-control cluepoint" @if(empty($clue->point)) value="0" @elseif($clue->point > 0) value="{{$clue->point}}" @else value="0" @endif min="0" max="300">
</div>


<div class="form-group winorlossdiv" @if($clue->type=='survey') style="display:none;" @endif>
    <!-- <label for="winorloss">Win or loss</label><br> --> 
    <!-- <input type="radio" name="winorloss" class="form-control winorloss" value="win">  -->
    <label class="radio-inline">
      <input type="radio" name="winorloss" value="win" @if($clue->status==1) checked @endif>Win
    </label>
    <label class="radio-inline">
      <input type="radio" name="winorloss" value="loss" @if($clue->status!=1) checked @endif>Loss
    </label>
</div>

<div class="form-group">
    <label for="category">Category</label>
    <select name="category" class="form-control" required>
        <option value="archive" @if($clue->category=="archive") selected @endif>Archive</option>
        <option value="collection" @if($clue->category=="collection") selected @endif>Collection</option>
    </select>
</div>

<div class="form-group">
    <input class="btn btn-primary" id="saveclue" type="submit" value="{{ $formMode == 'edit' ? 'Update' : 'Create' }}">
</div>


