@extends('layouts/admin.frontend_layout')

@section('content')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Indices</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            @if (session('success'))
            <div class="alert alert-success alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <p>{{ session('success') }}</p>
            </div>
            @endif
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Indices</h3>
                            <a href="{{ url('admin/clues/create') }}" title="Add"><button class="btn btn-primary pull-right"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Créer</button></a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                            <table id="example1" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Titre</th>
                                        <th>Indice</th>
                                        <th>Type</th>
                                        <th>Catégorie</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                @foreach($clues as $clue)
                                <tbody>
                                    <tr>
                                        <td>{{$clue->code}}</td>
                                        <td>{{$clue->title}}</td>
                                        <td>{!! $clue->clue !!}</td>
                                        <td>{{$clue->type}}</td>
                                        <td>{{$clue->category}}</td>
                                        <td>
                                            @if(!empty($clue->image))
                                            <img src="{{asset('uploads/'.$clue->image)}}" height="50px">
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{url('admin/clues/'.$clue->id.'/edit')}}" class="btn btn-primary">Editer</a>
                                        </td>
                                        <td>
                                            <form action="{{url('admin/clues/'.$clue->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Supprimer</button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                                @endforeach
                            </table>
                            </div>

                            {{$clues->links()}}
                        </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
<!-- /.content-wrapper --> 
@endsection