@extends('layouts/admin.frontend_layout')

@section('css')
<style>
form {
    padding: 30px;
}
.surveybox{
    border: 1px solid #d2d2d2;
    padding: 10px;
    border-radius: 4px;
    margin-bottom: 10px;
}
</style>
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Clues</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">  
                    <div class="box-body">
                        <div class="box box-info">
                            <div class="box-header with-border">
                              <h3 class="box-title">Create Clue</h3>

                                <a href="{{ url('/admin/clues') }}" title="Back"><button class="btn btn-warning btn-sm pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            </div>

                            <form method="POST" action="{{ url('/admin/clues') }}" accept-charset="UTF-8" enctype="multipart/form-data" id="clue-form">
                                {{ csrf_field() }}

                                @include ('admin.clues.form', ['formMode' => 'create'])
                            </form>                    
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
<script>
$(function() {

    $('#summernote').summernote();

    $("#clue-form").validate({
        rules: {
          code: "required",
          title: "required",
          ctype: "required"
        },
        messages: {
          code: "Please enter a code.",
          title: "Please enter a title.",
          ctype: "Please select clue type"
        }
    });

});

    jQuery(document).ready(function($) {
        $("#summernotediv").hide();
        $(".clupointdiv").hide();
        $(".surveybox").hide();
        $("#imagediv").hide();
        $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").hide();
        $(".winorlossdiv").hide();


        $(document).on('change','#cluecode',function(event) {
            event.preventDefault();
            
            var newval = $(this).val();
            var token = "{{csrf_token()}}";
            console.log(newval,token);
            
            if(newval!="" || newval!=null || newval != undefined)
            {
                $.ajax({
                    url: '{{route("checkcode")}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {'val':newval,'_token':token},
                })
                .done(function(data) {
                    console.log(data.data);
                    if(data.data == 'Already exists')
                    {
                        // $("#cluediv").append('<label id="code-error" class="error" for="code">Already exists.</label>');
                        alert("Clue already exists");
                        $("#saveclue").attr('disabled', true);
                    }else{
                        // $("#code-error").remove();
                        $("#saveclue").attr('disabled', false);
                    }
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
            }
            
            

        });

        $(document).on('blur', '.surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point', function(event) {            
            // $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").change(function(event) {
            console.log($(this).val());
            var question = $(".surveyquestion");
            var susans1 = $(".surveyanswer1");
            var susans2 = $(".surveyanswer2");
            var susans3 = $(".surveyanswer3");
            var susans4 = $(".surveyanswer4");
            var susright = $(".rightans");
            var suspoint = $(".point");
            var status = true;
            $(question).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            $(susans1).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            $(susans2).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            $(susans3).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            $(susans4).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

                

            $(susright).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            $(suspoint).each(function(index, el) {
                var elemval = $(el).val();
                if(elemval==null || elemval=="" || elemval==undefined){
                    console.log(elemval);
                    status = false;
                    $(".addmore").prop('disabled', 'true');
                }else{
                }
            });

            console.log(status);
            if(status==false)
            {
                console.log("false");
                $(".addmore").prop('disabled', true);
            }else{
                console.log("true");
                $(".addmore").prop('disabled', false);
            }

        });

        $(".addmore").click(function(event) {
            
            $(".surveyboxouter").append(`
                <div class="surveybox">

                <div class="form-group surveydiv">
                    <label for="surveyquestion">Clue Question :</label>
                    <input type="text" name="surveyquestion[]" class="form-control surveyquestion">
                </div>

                <div class="form-group surveydivans">
                    <label for="surveyanswer1">Clue Option 1 :</label>
                    <input type="text" name="surveyanswer1[]" class="form-control surveyanswer1"><br>

                    <label for="surveyanswer2">Clue Option 2:</label>
                    <input type="text" name="surveyanswer2[]" class="form-control surveyanswer2">

                    <label for="surveyanswer1">Clue Option 3 :</label>
                    <input type="text" name="surveyanswer3[]" class="form-control surveyanswer3"><br>

                    <label for="surveyanswer2">Clue Option 4:</label>
                    <input type="text" name="surveyanswer4[]" class="form-control surveyanswer4">
                </div>

                <div class="form-group surveydivansright">
                    <label for="rightans">Right Option</label>
                    <select name="rightans[]" class="form-control rightans">
                        <option value="option1">Option 1</option>
                        <option value="option2">Option 2</option>
                        <option value="option3">Option 3</option>
                        <option value="option4">Option 4</option>
                    </select>
                </div>

                <div class="form-group surveydivpoint">
                    <label for="points">Points</label>
                    <input type="number" name="points[]" class="form-control point" value="0" min="0" max="100">
                </div>
                
            </div>
            `);

            $(this).prop('disabled', true)

        });


        $("#ctype").change(function(event) {
            /* Act on the event */
            var selval = $(this).val();
            // alert(selval);
            if(selval=="text")
            {
                $("#summernotediv").show();
                $(".clupointdiv").show();
                $(".winorlossdiv").show();
                $("#imagediv").hide();
                $(".surveybox").hide();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").hide();

                $("#summernote").prop("required",true);
                $("#image").prop("required",false);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', false);

            }else if(selval=="image"){
                $("#summernotediv").hide();
                $(".clupointdiv").show();
                $("#imagediv").show();
                $(".winorlossdiv").show();
                $(".surveybox").hide();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").hide();

                $("#summernote").prop("required",false);
                $("#image").prop("required",true);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', false);

            }else if(selval=="textimage"){
                $("#summernotediv").show();
                $(".clupointdiv").show();
                $("#imagediv").show();
                $(".winorlossdiv").show();
                $(".surveybox").hide();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").hide();

                $("#summernote").prop("required",true);
                $("#image").prop("required",true);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', false);

            }else if(selval=="survey"){
                $("#summernotediv").hide();
                $(".clupointdiv").hide();
                $("#imagediv").hide();
                $(".winorlossdiv").hide();
                $(".surveybox").show();
                $(".surveydiv,.surveydivans,.surveydivansright,.surveydivpoint,.surveydivaddmore").show();

                $("#summernote").prop("required",false);
                $("#image").prop("required",false);
                $(".surveyquestion,.surveyanswer1,.surveyanswer2,.rightans,.point").prop('required', true);

            }
        });
    });
    $(document).ready(function(){ 
        $("#surveyquestion,#surveyanswer1,#surveyanswer2").on("keyup", function(){
            if($("#surveyquestion").val() != "" && $("#surveyanswer1").val() != "" && $("#surveyanswer2").val() != "" ){
                $("#clone_btn").removeAttr("disabled");
            }
        });

        $('#clone_btn').click(function(){
            $("#servey_parent").append($("#servey_QA").clone());
        });   
   
       //Image valiadation
        $("#image").change(function(){  
            $("#file_error").html("");
            $(".image").css("border-color","#F0F0F0");
            var file_size = $('#image')[0].files[0].size;
            if(file_size>2000000) {
                $("#file_error").html("<p style='color:#FF0000'>File size is greater than 2mb</p>");
                $(".image").css("border-color","#FF0000");
                return false;
            } 
                return true;
        });
    });
    
  
</script>
@stop