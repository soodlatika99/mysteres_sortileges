<div class="form-group">
    <label for="title">Titre :</label>
    <input type="text" name="title" class="form-control" value="@if(!empty($message)){{$message->title}}@endif" required>
</div>

<div class="form-group">
    <label for="date">Date :</label>
    <input type="date" name="date" class="form-control" value="@if(!empty($date)){{$message->date}}@endif" required>
</div>

<div class="form-group msgtimediv">
    <label for="date">Time :</label><br>
    <input type="time" name="msg_time" class="form-control">
</div>

<div class="form-group">
    <label for="description">Description :</label>
    <textarea id="summernote" name="description">@if(!empty($message)){{$message->description}}@endif</textarea>
</div>

<div class="form-group">
    <label for="date">One Day Event :</label><br>
    <input type="radio" name="odevent" value="yes" checked> Yes<br>
    <input type="radio" name="odevent" value="no"> No<br>
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode == 'edit' ? 'Mettre à jour' : 'Créer' }}">
</div>