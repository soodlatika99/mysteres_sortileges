@extends('layouts/admin.frontend_layout')

@section('css')
<style>
form {
    padding: 30px;
}
</style>
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Message</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body">
                        <div class="box box-info">
                            <div class="box-header with-border">
                              <h3 class="box-title">Create Message</h3>

                                <a href="{{ url('/admin/message') }}" title="Back"><button class="btn btn-warning btn-sm pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            </div>

                            <form method="POST" action="{{ url('/admin/message') }}" accept-charset="UTF-8" enctype="multipart/form-data" id="message-form">
                                {{ csrf_field() }}

                                @include ('admin.message.form', ['formMode' => 'create'])
                           </form>                    
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
<script>
$(function() {

    $('#summernote').summernote();

    $("#message-form").validate({
        rules: {
          title: "required",
          date: "required",
          description: "required",
        },
        messages: {
          title: "Please enter a title.",
          date: "Please select a date.",
          description: "Please enter a description.",
        }
    });

});
  
  $("input[name='odevent']").on('change', function(event) {
    event.preventDefault();
    /* Act on the event */
    var event = $(this).val();
    if(event=='yes')
    {
      $(".msgtimediv").show();
    }else{
      $(".msgtimediv").hide();
    }
  });

</script>
<style>
     label.error{
         color: red !important;
       }
</style>
@stop