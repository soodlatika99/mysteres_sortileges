@extends('layouts/admin.frontend_layout')

@section('css')
<style>
form {
    padding: 30px;
}
</style>
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Message</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body">
                        <div class="box box-info">
                            <div class="box-header with-border">
                              <h3 class="box-title">Editer Message</h3>

                                <a href="{{ url('/admin/message') }}" title="Back"><button class="btn btn-warning btn-sm pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                            </div>

                            <form method="POST" action="{{ url('/admin/message/'.$message->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" id="message-edit-form">
                                {{ csrf_field() }}
                                @method('PUT')

                                @include ('admin.message.form', ['formMode' => 'edit'])
                            </form>                    
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
<script>
$(function() {

    $('#summernote').summernote();

    $("#message-edit-form").validate({
        rules: {
          title: "required",
          date: "required",
          description: "required"
        },
        messages: {
          title: "Please enter a title.",
          date: "Please select a date.",
          description: "Please enter description "
        }
    });

});

jQuery(document).ready(function($) {
    $("#summernotediv").hide();
    $("#imagediv").hide();

    $("#description").change(function(event) {
        /* Act on the event */
        var selval = $(this).val();

        if(selval=="text")
        {
            $("#summernotediv").show();
            $("#imagediv").hide();
        }else if(selval=="image"){
            $("#summernotediv").hide();
            $("#imagediv").show();
        }else if(selval=="textimage"){
            $("#summernotediv").show();
            $("#imagediv").show();
        }
    });
});
</script>
<style>
     label.error{
         color: red !important;
       }
</style>
@stop