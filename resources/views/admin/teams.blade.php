@extends('layouts/admin.frontend_layout')

@section('content')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Teams</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            @if (session('success'))
            <div class="alert alert-success alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <p>{{ session('success') }}</p>
            </div>
            @endif
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Teams</h3>
                            <a href="{{url('admin/points/clear')}}" class="btn btn-success pull-right">Clear Points</a>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="table-responsive">
                            <table id="example1" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Team</th>
                                        <th>Points</th>
                                    </tr>
                                </thead>
                                @foreach($teams as $team)
                                <tbody>
                                    <tr>
                                        <td>{{$team->name}}</td>
                                        <td>{{$team->points}}</td>
                                    </tr>
                                </tbody>
                                @endforeach
                            </table>
                            </div>

                        </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
<!-- /.content-wrapper --> 
@endsection