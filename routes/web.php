<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/col', function () {
    return view('user.collections');
});

Route::get('/', 'User\IndexController@index');
Route::get('/login', 'User\IndexController@login');
Route::post('/postLogin', 'User\IndexController@postLogin');



Route::prefix('user')->middleware(['check_user'])->group(function () {
	Route::get('/home', 'User\IndexController@home');
	Route::get('/codes', 'User\CodeController@index');
	Route::post('/submitCode', 'User\CodeController@clue');
	Route::get('/archive', 'User\CodeController@archive');
	Route::get('/collections', 'User\CodeController@collections');
	Route::get('/messages', 'User\MessageController@messages');
	Route::get('/surveyque','User\SurveyController@survey');
	Route::get('/logout', 'User\IndexController@logout');
	Route::post('/savesurveyrunning', 'User\IndexController@savesurveyrunning')->name('savesurveyrunning');

	Route::post('/submitsurvey','User\SurveyController@submitsurvey')->name('submitsurvey');
	Route::get('/surveycompleted','User\SurveyController@surveycompleted')->name('surveycompleted');
});

Route::get('admin/login', 'Admin\IndexController@login');
Route::post('admin/postLogin', 'Admin\IndexController@postLogin');

Route::prefix('admin')->middleware(['check_admin'])->group(function () {
	Route::get('/dashboard', 'Admin\IndexController@index');
	Route::get('/teams', 'Admin\IndexController@team');
	Route::get('/logout', 'Admin\IndexController@logout');
	Route::resource('codes', 'Admin\CodeController');
	Route::resource('clues', 'Admin\ClueController');
	Route::resource('message', 'Admin\MessageController');
	Route::post('/checkcode', 'Admin\IndexController@checkcode')->name('checkcode');
	Route::get('/userexcel','Admin\IndexController@export')->name('userexcel');

	Route::get('/createuser','Admin\IndexController@createuser');
	Route::post('/newcodes','Admin\IndexController@newcodes');

	Route::get('/points/clear', 'Admin\IndexController@clearPoints');

	Route::get('/surveystatistics','Admin\IndexController@surveystatistics');
});

