<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyCompletedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_completeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('clue_id');
            $table->integer('user_id');
            $table->text('questions_answered');
            $table->string('total_points');
            $table->text('correct_answers');
            $table->text('points_gained');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_completeds');
    }
}
