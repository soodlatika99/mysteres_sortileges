/*09-04-2019*/
ALTER TABLE `users` CHANGE `password` `password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `users` ADD `role_id` INT(10) NOT NULL AFTER `id`;

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES (NULL, 'admin', NULL, NULL);

INSERT INTO `teams` (`id`, `name`, `created_at`, `updated_at`) VALUES (NULL, 'Becdaigle', NULL, NULL), (NULL, 'Tigredor', NULL, NULL), (NULL, 'Oursouffle', NULL, NULL), (NULL, 'Selezard', NULL, NULL);
ALTER TABLE `users` ADD `team_id` INT(10) NULL DEFAULT NULL AFTER `role_id`;