<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clue extends Model
{
    public function archive() {
        return $this->hasMany(Archive::class);
    }

    public function collection() {
        return $this->hasMany(Collection::class);
    }

    public function survey()
    {
    	return $this->hasMany(Survey::class);
    }

    public function surveycomplete()
    {
        return $this->hasMany(SurveyCompleted::class);
    }
}
