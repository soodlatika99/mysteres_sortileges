<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    public function clue() {
        return $this->belongsTo(Clue::class);
    }
}
