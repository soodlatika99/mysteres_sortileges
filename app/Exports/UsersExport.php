<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $users = User::get(['id','role_id','team_id','name','email','code','created_at']);
        return $users;
    }

    public function headings():array
    {
    	$titles = array();
    	$titles[] = 'ID';
    	$titles[] = 'Role id';
    	$titles[] = 'Team id';
    	$titles[] = 'Name';
    	$titles[] = 'Email';
    	$titles[] = 'Code';
    	$titles[] = 'Created at';

    	return $titles;
    }
}
