<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageReadStatus extends Model
{
	protected $fillable = ['user_id','message_id','status'];
    protected $table="message_read_statuses";
}
