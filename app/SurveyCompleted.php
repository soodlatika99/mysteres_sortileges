<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyCompleted extends Model
{
    
    public function clues()
    {
    	return $this->belongsTo(Clue::class);
    }
}
