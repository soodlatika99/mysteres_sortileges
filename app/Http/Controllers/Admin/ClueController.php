<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clue;
use App\Survey;

class ClueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clues = Clue::orderBy('created_at', 'DESC')->paginate(10);
        return view('admin.clues.list', ['clues' => $clues]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clues.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $image->move($destinationPath, $name);
            $image_name = $name;
        } else {
            $image_name = '';
        }

        $clue = new Clue();
        $clue->title = $request->title;
        $clue->code = $request->code;
        $clue->type = $request->ctype;
        $clue->category = $request->category;
        $clue->clue = $request->clue;
        $clue->point = (empty($request->point)) ? 0 : $request->point;
        $clue->image = $image_name;
        $clue->status = ($request->winorloss == 'win') ? 1 : 0;
        $clue->save();


        if($request->ctype=="survey")
        {
            for($i=0;$i<count($request->surveyquestion);$i++)
            {
                $survey = new Survey;
                $survey->clue_id = $clue->id;
                $survey->question = $request->surveyquestion[$i];
                $survey->option1 = $request->surveyanswer1[$i];
                $survey->option2 = $request->surveyanswer2[$i];
                $survey->option3 = $request->surveyanswer3[$i];
                $survey->option4 = $request->surveyanswer4[$i];
                $survey->rightoption = $request->rightans[$i];
                $survey->points = $request->points[$i];
                $survey->deduct = true;
                $survey->save();
            }
        }

        return redirect('admin/clues')->with('success', 'Clue created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clue = Clue::find($id);
        return view('admin.clues.edit', ['clue' => $clue]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $clue = Clue::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $image->move($destinationPath, $name);
            $image_name = $name;
        } else {
            if(!empty($request->already_image)) {
                $image_name = $request->already_image;
            } else {
                $image_name = '';
            }
        }

        $clue->title = $request->title;
        $clue->code = $request->code;
        $clue->type = $request->ctype;
        $clue->category = $request->category;
        $clue->clue = $request->clue;
        $clue->point = (empty($request->point)) ? 0 : $request->point;
        $clue->image = $image_name;
        $clue->status = ($request->winorloss == 'win') ? 1 : 0;

        $clue->save();

        // dd($request);

        if(!empty($request->surveyid))
        {

        for($i=0;$i<count($request->surveyid);$i++) {

            if($request->surveyid[$i] != null){

                $survey = Survey::find($request->surveyid[$i]);
                $survey->clue_id = $clue->id;
                $survey->question = $request->surveyquestion[$i];
                $survey->option1 = $request->surveyanswer1[$i];
                $survey->option2 = $request->surveyanswer2[$i];
                $survey->option3 = $request->surveyanswer3[$i];
                $survey->option4 = $request->surveyanswer4[$i];
                $survey->rightoption = $request->rightans[$i];
                $survey->points = $request->points[$i];
                $survey->deduct = true;
                $survey->save();

            }else{ 

                $survey = new Survey;
                $survey->clue_id = $clue->id;
                $survey->question = $request->surveyquestion[$i];
                $survey->option1 = $request->surveyanswer1[$i];
                $survey->option2 = $request->surveyanswer2[$i];
                $survey->option3 = $request->surveyanswer3[$i];
                $survey->option4 = $request->surveyanswer4[$i];
                $survey->rightoption = $request->rightans[$i];
                $survey->points = $request->points[$i];
                $survey->deduct = true;
                $survey->save();   
            }
        }
        
        }else{
            $survey = Survey::where('clue_id',$id)->delete();
        }

        return redirect('admin/clues')->with('success', 'Clue updated successfully.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clue = Clue::find($id);
        $clue->delete();

        return redirect('admin/clues')->with('success', 'Clue deleted successfully.');
    }
}
