<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Team;
use App\ClueCodeUse;
use Auth;

class CodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role_id', 2)->orderBy('created_at', 'DESC')->paginate(10);
        $cluecodeuse = ClueCodeUse::with('clue')->get();
        $winarr = 0;
        $lossarr = 0;

        foreach ($users as $user) {
            $winarr = 0;
            $lossarr = 0;
            foreach ($cluecodeuse as $cluecode) {
                if($cluecode->user_id == $user->id)
                {
                    if($cluecode->clue['status']==1){
                        $winarr += $cluecode->clue['point'];
                    }else{
                        $lossarr += $cluecode->clue['point'];
                    }
                }
            }
            $actualpoint = $winarr - $lossarr;
            $user->setAttribute('points',$actualpoint);
        }

        return view('admin.codes.list', ['users' => $users,'points' => $actualpoint]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::all();
        return view('admin.codes.create', ['teams' => $teams]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $user = new User();

       //$user->name = $request->name;
       $user->role_id = 2;
       $user->team_id = $request->team;
       //$user->email = $request->email;
       $user->code = $request->code;
       $user->save();

       return redirect('admin/codes')->with('success', 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teams = Team::all();
        $user = User::find($id);
        return view('admin.codes.edit', ['teams' => $teams, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
       // $user->name = $request->name;
        //$user->email = $request->email;
        $user->code = $request->code;
        $user->team_id = $request->team;
        $user->save();

        return redirect('admin/codes')->with('success', 'User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('admin/codes')->with('success', 'User deleted successfully.');
    }
}
