<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Clue;
use Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App\Team;
use App\User;
use App\ClueCodeUse;
use App\SurveyCompleted;
use DB;
use App\Survey;

class IndexController extends Controller
{
    public function index()
    {
    	return view('admin.index');
    }

    public function login()
    {
    	return view('admin.login');
    }

    public function postLogin(Request $request)
    {
    	if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

    		return redirect('admin/dashboard');
		} else {
			return redirect()->back()->with('error', 'Incorrect email or password.');
		}
    }

    public function logout()
    {
    	Auth::logout();
        return redirect('admin/login');
    }

    public function checkcode(Request $req)
    {
        $val = $req->val;
        $check = Clue::where('code',$val)->count();
        if($check > 0)
        {
            return Response::json([ 'data'=>'Already exists' ]);
        }else{
            return Response::json([ 'data'=>'Can be inserted' ]);
        }
    }

    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function team(Request $req)
    {
        $users = User::where('role_id', 2)->orderBy('created_at', 'DESC')->get();
        $teams = Team::with('user.clueused.clue')->get();
        $cluecodeuse = ClueCodeUse::with('clue')->get();
        $winarr = 0;
        $lossarr = 0;

        

       // $usersclue = ClueCodeUse::pluck('id')->toArray();
        foreach ($teams as $team) {
            $teampoint = 0;
            foreach ($team->user as $user) {
                // dd($user);
                $players = (!empty($user->no_of_player)) ? (int) $user->no_of_player : 1;
                $winarr = 0;
                $lossarr = 0;

                foreach ($user->clueused as $cluecode) {
                    
                    if($cluecode->clue['status']==1 && $cluecode->clue['code']!="py39n6"){
                        // $winarr += $cluecode->clue['point'];
                        if($cluecode->clue['type'] == 'survey')
                        {
                            $surveycompleted = SurveyCompleted::where('clue_id',$cluecode->clue_id)->where('user_id',$cluecode->user_id)->first();
                            if($surveycompleted)
                            {
                                $winarr += $surveycompleted->total_points * $players;
                            }
                        }else{
                            $winarr += $cluecode->clue['point'] * $players;
                        }

                    }else{
                        $lossarr += $cluecode->clue['point'];
                    }
                    
                }

                $actualpoint = $winarr - $lossarr;
                $teampoint = $teampoint + $actualpoint;
                $user->setAttribute('points',$actualpoint);

            }
            $team->setAttribute('points',$teampoint);
        }
        // dd($teams);


        return view('admin.teams', ['teams' => $teams]);
    }

   
   //All team point set 0
    public function clearPoints()
    {
        DB::table('clue_code_uses')->truncate();
        DB::table('survey_completeds')->truncate();
        return redirect()->back();
    }

    public function createuser()
    {
        return view('admin.codes.newuser');
    }

    public function newcodes(Request $req)
    {
        $teams = Team::all()->pluck('id')->toArray();

        $players = (int) $req->nouser;
        if($req->nouser > 0 && $req->nouser<=50)
        {
            $string = str_random(4);
            for($i=0;$i<$players;$i++)
            {
                $user = User::where('code',$string)->first();

                if($user==null)
                { 
                    $user = new User;
                    $user->code = $string;
                    $user->role_id = 2;
                    $user->no_of_player = '';
                    $user->team_id = array_rand($teams,1)+1;
                    $user->save();
                    
                }else{

                }
            }
        }

        return redirect()->back()->with('success','Users created successfully');
    }

    public function surveystatistics(Request $req)
    {
       
        $clues = Clue::where('type','survey')->get();
        $clueid = ($req->clues) ? $req->clues : '';

        $statisticarray = array();
        if($req->clues)
        {
            
            $clue = Clue::with('surveycomplete')->where('id',$req->clues)->first();
            
            $survey = Survey::where('clue_id',$req->clues)->get();
            foreach ($survey as $surve) {
                $statisticarray[$surve->id]['question'] = $surve->question;
                $statisticarray[$surve->id]['correct_answers'] = 0;
                $statisticarray[$surve->id]['users_answered'] = 0;
            }
            // dd($statisticarray);

            
            $surveycompleted = SurveyCompleted::where('clue_id',$req->clues)->get();
            // dd($surveycompleted);
            foreach ($surveycompleted as $surveycomplete) {
                $questions = json_decode($surveycomplete->questions_answered);
                // dd($questions);
                $correct_answers = json_decode($surveycomplete->correct_answers);
                $points_gained = json_decode($surveycomplete->points_gained);
                

                $myquestions = array_combine($questions, $correct_answers);
                foreach ($myquestions as $key=>$value) {
                    $statisticarray[$key]['users_answered']++;
                    if($value==1)
                    {
                        $statisticarray[$key]['correct_answers']++;
                    }
                }
                $ques = 0;
            }
        }
                
        // dd($statisticarray);

        return view('admin.surveystatistics')->with('clues',$clues)->with('clueid',$clueid)->with('statisticsarray' , $statisticarray);
    }
}