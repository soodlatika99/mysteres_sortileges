<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Message;
use App\MessageReadStatus;
use App\ClueCodeUse;
use App\Clue;
use DB;
use Carbon\Carbon;
use Response;
use App\SurveyRunning;
use App\Survey;
use App\SurveyCompleted;
use Illuminate\Support\Arr;

class IndexController extends Controller
{
    public function index()
    {
    	return view('user.index');
    }

    public function home()
    {
        $currentTime = date('H:i:s'); 
        $current = Carbon::today();
        $currentDate =  $current->toDateString();  
       // $now = Carbon::now(); 
        // dd($now->toDateString(),$now->toTimeString(),$message);
        // $now = Carbon::today();
        // $todaydate = $now->toDateString(); 
       $message = Message::where('one_day_event','=','no')->orWhere(function ($query)
        use($currentDate){
           $query->where('one_day_event','=','yes')->where('date','<=',$currentDate);
        })->where('msg_time','<=',$currentTime)->get();

        // $message = Message::where('date','<=',$now->toDateString())->where('msg_time','<=',$now->toTimeString())->get();
        $messageReadStatus = MessageReadStatus::where('user_id',Auth::id())->get(); 
        $messageids = $message->pluck('id')->toArray();
        $messagereadids = $messageReadStatus->pluck('message_id')->toArray();
        $diffarray = array(); 
        foreach ($messageids as $value) {
            # code...
            if(!in_array($value, $messagereadids))
            {
                $diffarray[] = $value;
            }
        }   
        // Auth::logout();
        $tusers = User::has('clueused')->where('id',Auth::id())->where('users.role_id',2)->get();     // with(['clueused'=>function ($query){ $query->where('clue_id','<>',''); }])->
        // ->where('users.team_id',Auth::user()->team_id)->
        $clue = Clue::all();
        $tpoints = 0;
        foreach($tusers as $user)
        {
            $cluecodeuse = ClueCodeUse::with('clue')->where('user_id',$user->id)->get();
            $winarr = 0;
            $lossarr = 0;
            $jackpotpoint = 0;

            foreach ($cluecodeuse as $cluecode) {
                if($cluecode->clue['status']==1 && $cluecode->clue['code']!="py39n6"){
                    if($cluecode->clue['type'] == 'survey')
                    {
                        $surveycompleted = SurveyCompleted::where('clue_id',$cluecode->clue_id)->where('user_id',$cluecode->user_id)->first();
                        if($surveycompleted)
                        {
                            $winarr += $surveycompleted->total_points;
                        }
                    }else{
                        $winarr += $cluecode->clue['point'];
                    }
                }elseif($cluecode->clue['code']=="py39n6"){
                    $jackpotpoint = (int) $cluecode->clue['point'];
                }else{
                    $lossarr += $cluecode->clue['point'];
                }

            }
            $actualpoint = ($user->no_of_player!=0) ? ( ($winarr - $lossarr) * (int) $user->no_of_player ) : ($winarr - $lossarr);
            $actualpoint = (!empty($jackpotpoint)) ? $actualpoint+$jackpotpoint : $actualpoint;

            $tpoints = $tpoints + $actualpoint;
        }
        return view('user.home',compact('message','diffarray','tpoints'));
    }

    public function login()
    {
        if(Auth::check()) {
            return redirect('user/home');
        } else {
            return view('user.login');
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    public function postLogin(Request $request)
    {
        $user = User::where('code', $request->code)->first();
    	if (!empty($user)) {
            auth()->login($user, true);
            DB::update('update users set no_of_player = ? where id = ?',[$request->players,Auth::id()]);
    		return redirect('user/home');
		} else {
			return redirect()->back()->with('error', 'Code not valid.');
		}
    }

    public function savesurveyrunning(Request $request)
    {
        $survey = Survey::where('id',$request->question)->first();
        $surveyrunning = SurveyRunning::where('user_id',Auth::id())->where('clue_id',$survey->clue_id)->first();

        if($request->has('answer'))
        {
           
            if(!empty($surveyrunning))
            {
                $questions_answered1 = json_decode($surveyrunning->questions_answered);
                array_push($questions_answered1, $request->question);
                
                $points_gained1 = json_decode($surveyrunning->points_gained);
                

                $correct_answers1 = json_decode($surveyrunning->correct_answers);

                $surveyrunning->questions_answered = json_encode($questions_answered1);
                // return gettype($questions_answered);

                $surveyrunning->last_question_answered = $request->question;
                $surveyrunning->status = 'incomplete';
                if($survey->rightoption==$request->answer)
                {
                    // $surveyrunning->questions_answered = $surveyrunning->questions_answered + 1;
                    // $surveyrunning->points_gained = $surveyrunning->points_gained + $survey->points;
                    array_push($points_gained1,$survey->points);
                    $surveyrunning->points_gained = json_encode($points_gained1);
                    // $surveyrunning->correct_answers = $surveyrunning->correct_answers+1;
                    array_push($correct_answers1,1);
                    $surveyrunning->correct_answers = json_encode($correct_answers1);
                    // return $surveyrunning;
                }else{
                    array_push($points_gained1,0);
                    array_push($correct_answers1,0);
                    $surveyrunning->points_gained = json_encode($points_gained1);
                    $surveyrunning->correct_answers = json_encode($correct_answers1);
                }
                $surveyrunning->save();
            }else{
                $surveyrunning = new SurveyRunning;
                $surveyrunning->user_id = Auth::id();
                $surveyrunning->clue_id = $survey->clue_id;
                // $surveyrunning->questions_answered = 1;
                $surveyrunning->questions_answered = json_encode(array($request->question));

                if($survey->rightoption==$request->answer)
                {
                    // $surveyrunning->points_gained = $survey->points;
                    $surveyrunning->points_gained = json_encode(array($survey->points));
                    // $surveyrunning->correct_answers = $surveyrunning->correct_answers+1;
                    $surveyrunning->correct_answers = json_encode(array(1));
                }else{
                    // $surveyrunning->points_gained = 0;
                    $surveyrunning->points_gained = json_encode(array(0));
                    $surveyrunning->correct_answers = json_encode(array(0));
                }
                $surveyrunning->last_question_answered = $request->question;
                $surveyrunning->status = 'incomplete';
                $surveyrunning->save();
            }
            return Response::json(['data'=>$request->all()]);
        }else{
            /*
            if(!empty($surveyrunning))
            {
                
                $surveyrunning->questions_answered = $surveyrunning->questions_answered + 1;
                $surveyrunning->points_gained = 0;
                $surveyrunning->last_question_answered = $request->question;
                $surveyrunning->status = 'incomplete';
                $surveyrunning->save();
                
            }else{
                $surveyrunning = new SurveyRunning;
                $surveyrunning->user_id = Auth::id();
                $surveyrunning->clue_id = $survey->clue_id;
                $surveyrunning->questions_answered = 1;                
                $surveyrunning->points_gained = 0;
                $surveyrunning->last_question_answered = $request->question;
                $surveyrunning->status = 'incomplete';
                $surveyrunning->save();
            }
            */
            return Response::json(['data'=>'No answer is selected']);
        }
    }
}
