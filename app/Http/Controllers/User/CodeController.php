<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clue;
use App\ClueCodeUse;
use App\Archive;
use App\Collection;
use App\Message;
use App\Survey;
use App\SurveyRunning;
use DB;
use Auth;

class CodeController extends Controller
{
    public function index()
    {
    	return view('user.codes');
    }
    
    public function subclue(Request $request)
    {
        return view('user.clues');
    }

    public function clue(Request $request)
    {
    	$clue = Clue::where('code', $request->code)->first();    

        if(!empty($clue)) {
            $clueUses = ClueCodeUse::where('clue_id', $clue->id)->where('user_id',Auth::id())->first();

            if($request->code == "py39n6"){

                $clueUses = ClueCodeUse::where('clue_id', $clue->id)->first();
            }
            if(empty($clueUses)) {

                if($clue->type!='survey')
                {
                    $useClue = new ClueCodeUse();
                    $useClue->clue_id = $clue->id;
                    $useClue->user_id = Auth::user()->id;
                    $useClue->save();
                }

                if($clue->category=="archive" && $clue->type != "survey") {
                    if($clue->type == "image"){
                        $useClue = new Collection();
                        $useClue->clue_id = $clue->id;
                        $useClue->user_id = Auth::user()->id;
                        $useClue->save(); 
                    }else{
                        $useClue = new Archive();
                        $useClue->clue_id = $clue->id;
                        $useClue->user_id = Auth::user()->id;
                        $useClue->save();
                    }
                }  
                if($clue->category=="collection"){
                    $useClue = new Collection();
                    $useClue->clue_id = $clue->id;
                    $useClue->user_id = Auth::user()->id;
                    $useClue->save();
                }
                if($clue->type=='survey')
                {
                    $surveyrunning = SurveyRunning::where('clue_id',$clue->id)->where('user_id',Auth::id())->first();
                    if(!empty($surveyrunning))
                    {
                        $surveyques = Survey::where('clue_id',$clue->id)->where('id','>',$surveyrunning->last_question_answered)->orderBy('id','asc')->get();
                    }else{
                        $surveyques = Survey::where('clue_id',$clue->id)->orderBy('id','asc')->get();
                    }
                    if(!empty($surveyques) && $surveyques->count() > 0)
                    {
                        return view('user.survey_question')->with('questions',$surveyques);
                    }else{
                        return "No questions found for this clue";
                    }
                }
                return view('user.clues', ['clue' => $clue]);

            } else {
                return redirect()->back()->with('error', 'Code déjà utilisé. Vous pouvez le retrouver dans vos archives.');
            }
        } else {
            return redirect()->back()->with('error', 'Code non valide.');
        }
    }

    public function archive()
    {
        $archives = Archive::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        return view('user.archive', ['archives' => $archives]);
    }

    public function collections()
    {
        $collections = Collection::where('user_id', Auth::user()->id)->get();
        return view('user.collections', ['collections' => $collections]);
    }

    
    public function survey()
    {
      $questions = Survey::all();   
      return view('user.survey_question',compact('questions'));
    }
}
