<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clue;
use App\Survey;
use App\SurveyRunning;
use App\ClueCodeUse;
use Auth;
use App\SurveyCompleted;

class SurveyController extends Controller
{
    public function survey(Request $req)
    {
    	$clue = Clue::find(18);
    	$survey = Survey::where('clue_id',$clue->id)->get();

    	return view('user.survey_question')->with('questions',$survey);

    }

    public function submitsurvey(Request $req)
    {
    	$points = 0;
    	$clue = 0;
    	$correctques = 0;
    	$surveyrunning = null;
    	$lquestion = 0;
    	foreach($req->squestion as $question)
    	{
    		$surveyques = Survey::find($question);
    		$lquestion = $question;
    		if($req->sanswer[$question] == $surveyques->rightoption)
    		{
    			$points = $surveyques->points;
    			// $correctques++;
    		}

    		$clue = $surveyques->clue_id;
	    	$surveyrunning = SurveyRunning::where('user_id',Auth::id())->where('clue_id',$clue)->first();
    	}

    	// dd($points,$lquestion);

    	if(!empty($surveyrunning))
    	{
    		$codeclueuse = new ClueCodeUse;
    		$codeclueuse->clue_id = $clue;
    		$codeclueuse->user_id = Auth::id();
    		$codeclueuse->status = 'Completed';
    		$codeclueuse->save();
    		$surveyrunningquestions = json_decode($surveyrunning->questions_answered);
    		array_push($surveyrunningquestions, $question);

    		$surveyrunningpoints = json_decode($surveyrunning->points_gained);
    		array_push($surveyrunningpoints, $points);

    		$lastquestionright = (!empty($points)) ? 1 : 0 ;
    		$surveyrunninganswers = json_decode($surveyrunning->correct_answers);
    		array_push($surveyrunninganswers, $lastquestionright);


    		$surveycompleted = new SurveyCompleted;
    		$surveycompleted->clue_id = $surveyrunning->clue_id;
    		$surveycompleted->user_id = $surveyrunning->user_id;
    		$surveycompleted->questions_answered = json_encode($surveyrunningquestions);
    		$surveycompleted->total_points = array_sum($surveyrunningpoints);
    		$surveycompleted->correct_answers = json_encode($surveyrunninganswers);
    		$surveycompleted->points_gained = json_encode($surveyrunningpoints);
    		$surveycompleted->status = 'Completed';
    		$surveycompleted->save();

    	}else{
    		$codeuse = ClueCodeUse::where('clue_id',$clue)->where('user_id',Auth::id())->first();
    		if(!empty($codeuse))
    		{
    			$codeuse->status = $points;
    			$codeuse->save();
    		}else{

    		}
    	}

    	$surveyrunning->delete();

    	return view('user.surveycompleted')->with('message', "You have answered ".array_sum($surveyrunninganswers)." questions correctly.You won ".array_sum($surveyrunningpoints)." points.");
    }

    public function surveycompleted(Request $req)
    {
    	// return view('user.surveycomplated')->
    }
}
