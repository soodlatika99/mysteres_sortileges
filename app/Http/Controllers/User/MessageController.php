<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MessageReadStatus;
use App\Message;
use Auth;
use Carbon\Carbon;


class MessageController extends Controller
{
    public function messages(Request $request)
    {
        //current date    
        //$time = Carbon::now();
        $currentTime = date('H:i:s'); 
        $current = Carbon::today();
        $currentDate =  $current->toDateString();  
        $messageCreates = Message::orderBy('created_at', 'desc')->get(); 
        $messages = Message::orderBy('created_at', 'desc')->get();
        
        $displayMessages=array();

        // dd($currentDate);
        foreach($messageCreates as $messageCreate){
            if($messageCreate->one_day_event == "no" || $messageCreate->one_day_event == ""){
                array_push($displayMessages,$messageCreate);
            }
            if($messageCreate->one_day_event == "yes"){
                if($currentDate == $messageCreate->date) {
                    if($currentTime >= $messageCreate->msg_time){
                      array_push($displayMessages,$messageCreate);  
                    }
                }else if($currentDate < $messageCreate->date){
                    //code

                }else if($currentDate >= $messageCreate->date){
                    
                    $del=Message::where('id',$messageCreate->id)->delete(); 
                }
            }
        }

        $messagesids = $messages->pluck('id');
        foreach ($messagesids as $value) {
        	$messagestatus = MessageReadStatus::updateOrCreate(
        		[
        			'user_id'=>Auth::id(),
        			'message_id' => $value
        		],
        		[
		    		'user_id' => Auth::id(), 
		    		'message_id' => $value, 
		    		'status' => 'read'
	    		]);
        }
    	
        // $messages = Message::orderBy('created_at', 'DESC')->get();
        return view('user.messages',['messages' => $displayMessages]);
    }
}
