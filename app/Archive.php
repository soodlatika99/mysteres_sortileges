<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{
    public function clue() {
        return $this->belongsTo(Clue::class);
    }
}
