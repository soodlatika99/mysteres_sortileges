<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClueCodeUse extends Model
{
    protected $table = "clue_code_uses";

    public function clue()
    {
    	return $this->hasOne(Clue::class,'id','clue_id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class,'user_id','id');
    }
}
